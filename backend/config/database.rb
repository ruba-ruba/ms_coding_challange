# frozen_string_literal: true

require 'sequel'

class Database
  def initialize(options)
    @options = options
  end

  def connect
    Sequel.connect(@options.fetch(:uri), @options.except(:uri))
  end
end
