# frozen_string_literal: true

require 'active_support/all'
require 'contentful'
require 'dry-struct'
require 'grape-entity'
require 'grape-swagger'
require 'grape-swagger/entity'
require 'grape-swagger/representable'
require 'grape_logging'
require 'grape'
require 'require_all'
require 'yaml'

require_relative 'errors'
require_relative 'env'
require_relative 'database'

class Application
  include Singleton

  def env
    @env ||= ENV
  end

  def initialize!
    initialize_logger
    require_app
    require_libs
  end

  private

  def initialize_logger; end

  def require_app
    require_all 'app/models'
    require_all 'app/entities'
    require_all 'app/helpers'
    require_all 'app/controllers'
    require_all 'app/services'
    require_all 'app/resources'
  end

  def require_libs; end

  def require_dependency(path)
    require_relative File.join('../', path)
  end

  def load_config(file_name)
    path = File.join('config', file_name)
    yaml = ERB.new(File.read(path)).result
    config = YAML.safe_load(yaml, [], [], true).symbolize_keys
    config = config[env['RACK_ENV'].to_sym] || {}
    config.symbolize_keys
  end

  class << self
    def method_missing(method, *args, &block)
      if instance.respond_to?(method)
        instance.send(method, *args, &block)
      else
        super
      end
    end

    def respond_to_missing?(method, include_private = false)
      instance.respond_to?(method) || super
    end
  end
end
