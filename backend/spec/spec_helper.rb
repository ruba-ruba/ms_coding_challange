# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'
unless ENV['SKIP_COVERAGE'] == '1'
  require 'simplecov'
  SimpleCov.start do
    add_filter '/bin'
    add_filter '/db/migrations'
    add_filter '/spec'
  end
end

require 'rspec'

require_relative '../app'

require 'factory_bot'
require 'rack/test'
require 'webmock'
require 'webmock/rspec'
require 'pry'
require 'vcr'

Dir[File.join(File.dirname(__FILE__), 'support', '*.rb')].sort.each { |file| require file }

VCR.configure do |c|
  c.cassette_library_dir = 'spec/support/cassettes'
  c.hook_into :webmock
  c.default_cassette_options = { record: :new_episodes }
  c.configure_rspec_metadata!
end

RSpec.configure do |config|
  config.include Rack::Test::Methods, type: :controller
  config.include SpecSupport::Controllers, type: :controller
  config.include SpecSupport::CommonHelpers

  config.include FactoryBot::Syntax::Methods
  FactoryBot.find_definitions

  config.before(:suite) do
    FactoryBot.factories.reject do |factory|
      factory.name == :base
    end
  end

  config.around(:each, &:run)

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
end
