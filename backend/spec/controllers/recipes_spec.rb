# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Controllers::Recipes, type: :controller do
  let(:app) { described_class }

  describe 'GET /recipes' do
    let(:recipe1) { build(:recipe) }
    let(:recipe2) { build(:recipe) }

    let(:expected_response) do
      { collection:
        [
          { id: recipe1.id, title: recipe1.title, image_url: recipe1.image_url },
          { id: recipe2.id, title: recipe2.title, image_url: recipe2.image_url }
        ] }.deep_stringify_keys
    end

    subject { get '/recipes' }

    before do
      allow(Services::Recipes.instance).to receive(:list) { [recipe1, recipe2] }
    end

    it { is_expected.to be_ok }

    it 'responds with recipes' do
      subject
      expect(response_body).to eq expected_response
    end
  end

  describe 'GET /recipes/:id' do
    let(:recipe) { build(:recipe) }
    let(:id) { recipe.id }
    let(:response) do
      {
        id: recipe.id,
        title: recipe.title,
        image_url: recipe.image_url,
        tags: recipe.tags,
        description: recipe.description,
        chef_name: recipe.chef_name
      }.stringify_keys
    end

    subject { get "/recipes/#{id}" }

    before do
      allow(Services::Recipes.instance).to receive(:get).with(id) { recipe }
    end

    it { is_expected.to be_ok }

    it 'responds with recipes' do
      subject
      expect(response_body).to eq response
    end
  end
end
