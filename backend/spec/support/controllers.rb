# frozen_string_literal: true

require 'jwt'

module SpecSupport
  module Controllers
    def response_body
      obj = JSON.parse(last_response.body)
      obj = obj.with_indifferent_access if obj.respond_to?(:with_indifferent_access)
      obj
    end
  end
end
