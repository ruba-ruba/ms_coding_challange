require 'spec_helper'

RSpec.describe ::Services::Recipes do
  let(:instance) { described_class.instance }
  let(:client) { instance_double('Contentful::Client') }

  before do
    allow(::Contentful::Client).to receive(:new) { client }
  end

  describe '#list' do
    subject(:list) { instance.list }

    specify do
      expect(client).to receive(:entries).with(content_type: 'recipe').once.and_return(double.as_null_object)
      list
    end
  end

  describe '#get' do
    let(:id) { 'dummy' }
    subject(:get) { instance.get(id) }

    specify do
      expect(client).to receive(:entry).with(id, content_type: 'recipe').once
      get
    end
  end
end
