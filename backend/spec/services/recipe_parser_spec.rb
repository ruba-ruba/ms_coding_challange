require 'spec_helper'

RSpec.describe ::Services::RecipeParser do
  CONTENTFUL_RECIPE = Struct.new(:id, :title, :description, :chef, :photo, :tags) do
    def fields
      to_h
    end
  end
  CONTENTFUL_TAG = Struct.new(:name)
  CONTENTFUL_ASSET = Struct.new(:url)
  CONTENTFUL_CHEF = Struct.new(:name)

  let(:contentful_recipe) do
    tags = [CONTENTFUL_TAG.new('name1'), CONTENTFUL_TAG.new('name2')]
    chef = CONTENTFUL_CHEF.new('chef')
    photo = CONTENTFUL_ASSET.new('http://easter_egg.for.spec_readers')
    CONTENTFUL_RECIPE.new('uuid', 'title', 'descr', chef, photo, tags)
  end

  describe '#parse' do
    subject { described_class.new(contentful_recipe).parse }

    specify do
      expected = ::Resources::Recipe.new(
        id: contentful_recipe.id,
        title: contentful_recipe.title,
        description: contentful_recipe.description,
        image_url: contentful_recipe.photo.url,
        tags: %w[name1 name2],
        chef_name: 'chef'
      )
      expect(subject).to eq expected
    end
  end
end
