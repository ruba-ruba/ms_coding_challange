# frozen_string_literal: true

FactoryBot.define do
  factory :base do
    to_create(&:save)
  end
end
