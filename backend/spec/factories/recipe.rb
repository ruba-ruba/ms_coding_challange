FactoryBot.define do
  factory :recipe, class: Resources::Recipe do
    initialize_with do
      new(attributes)
    end

    sequence(:id, &:to_s)
    sequence(:title) { |n| "title-#{n}" }
    image_url { 'https://recipe.image.url' }
    description { 'recipe description' }
    tags { %w[tag1 tag2] }
    chef_name { 'Best Chef' }
  end
end
