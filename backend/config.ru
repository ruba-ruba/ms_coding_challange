# frozen_string_literal: true

require_relative 'config/boot'

require 'pinglish'

use Pinglish do |ping|
  ping.check :db do
    Application.db.tables.size
  end
end

require 'rack-request-id'

request_id_generator = proc do
  uuid = SecureRandom.uuid
  Thread.current[:root_span]&.set_tag(:uuid, uuid)
  uuid
end

use Rack::RequestId, id_generator: request_id_generator
run Controllers::API
