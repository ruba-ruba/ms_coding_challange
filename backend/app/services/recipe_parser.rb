module Services
  class RecipeParser
    def initialize(recipe)
      @recipe = recipe
    end

    def parse
      ::Resources::Recipe.new(
        id: recipe.id,
        title: recipe.title,
        description: recipe.description,
        image_url: recipe.photo.url,
        tags: format_tags,
        chef_name: format_chef_name
      )
    end

    private

    attr_reader :recipe

    def format_tags
      return unless recipe.fields[:tags]

      recipe.tags.map(&:name)
    end

    def format_chef_name
      return unless recipe.fields[:chef]

      recipe.chef.name
    end
  end
end
