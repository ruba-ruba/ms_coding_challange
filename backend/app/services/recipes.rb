module Services
  class Recipes
    include Singleton

    CONTENT_TYPE = 'recipe'.freeze

    def initialize
      @space_id = ENV['SPECE_ID']
      @environment_id = ENV['ENVIRONMENT_ID']
      @access_token = ENV['ACCESS_TOKEN']
    end

    def list
      client.entries(options).map do |recipe|
        ::Services::RecipeParser.new(recipe).parse
      end
    end

    def get(id)
      recipe = client.entry(id, options)
      return unless recipe

      ::Services::RecipeParser.new(recipe).parse
    end

    private

    attr_reader :space_id, :environment_id, :access_token

    def client
      # no need to do memoization with @client ||=
      # we have Singleton here :)
      Contentful::Client.new(
        access_token: access_token,
        space: space_id,
        environment: environment_id
      )
    end

    def options
      { content_type: CONTENT_TYPE }
    end
  end
end
