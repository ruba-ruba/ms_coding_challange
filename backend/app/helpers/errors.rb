# frozen_string_literal: true

module Helpers
  module Errors
    extend Grape::API::Helpers

    def present_exception(exception, status = 500)
      error = Entities::Error.represent(exception)
      error!(error.serializable_hash, status)
    end
  end
end
