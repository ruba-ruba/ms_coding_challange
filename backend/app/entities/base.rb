# frozen_string_literal: true

module Entities
  class Base < Grape::Entity
    format_with :iso_timestamp do |time|
      time&.iso8601
    end
  end
end
