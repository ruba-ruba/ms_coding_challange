# frozen_string_literal: true

module Entities
  class Error < Base
    expose :type do |e|
      type = e.type if e.respond_to?(:type)
      type || e.class.name
    end
    expose :errors, safe: true, documentation: { type: 'object' }
  end
end
