require_relative 'base'

module Entities
  class RecipeDetails < Base
    expose :id, documentation: { type: 'Int', desc: 'Recipe ID' }
    expose :title, documentation: { type: 'String', desc: 'Recipe title' }
    expose :image_url, documentation: { type: 'String', desc: 'Recipe image URL' }
    expose :tags, documentation: { type: 'Array', desc: 'Recipe tags' }
    expose :description, documentation: { type: 'String', desc: 'Recipe description' }
    expose :chef_name, documentation: { type: 'String', desc: 'Recipe Chef Name' }
  end
end
