require_relative 'base'

module Entities
  class Recipe < Base
    expose :id, documentation: { type: 'Int', desc: 'Recipe ID' }
    expose :title, documentation: { type: 'String', desc: 'Recipe title' }
    expose :image_url, documentation: { type: 'String', desc: 'Recipe image URL' }
  end
end
