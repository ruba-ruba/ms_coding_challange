require_relative 'types'

module Resources
  class Recipe < Dry::Struct
    attribute :id, Types::String
    attribute :title, Types::String
    attribute :description, Types::String
    attribute :image_url, Types::String
    attribute :tags, Types::Array.optional
    attribute :chef_name, Types::String.optional
  end
end
