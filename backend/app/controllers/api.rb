# frozen_string_literal: true

require_relative 'recipes'

module Controllers
  class API < Grape::API
    default_format :json
    format :json

    logger.formatter = GrapeLogging::Formatters::Logstash.new
    use GrapeLogging::Middleware::RequestLogger, { logger: logger }

    helpers ::Helpers::Errors

    rescue_from Errors::NotFound do |e|
      present_exception(e, 404)
    end

    mount ::Controllers::Recipes

    add_swagger_documentation base_path: ''
  end
end
