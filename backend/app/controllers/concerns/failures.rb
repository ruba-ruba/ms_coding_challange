# frozen_string_literal: true

module Controllers
  module Failures
    extend ActiveSupport::Concern

    module ClassMethods
      def request_failures
        bad_request_failures
      end

      def bad_request_failures
        [{ code: 400,
           message: 'Bad Request',
           model: Entities::Error },
         { code: 404,
           message: 'Not Found',
           model: Entities::Error }]
      end
    end
  end
end
