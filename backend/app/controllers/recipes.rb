require_relative 'concerns/failures'

module Controllers
  class Recipes < Grape::API
    default_format :json

    resource :recipes do
      include Failures

      helpers do
        def recipes_service
          Services::Recipes.instance
        end
      end

      before do
        header['Access-Control-Allow-Origin'] = '*'
        header['Access-Control-Request-Method'] = '*'
        header['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, PATCH, DELETE'
        header['Access-Control-Allow-Headers'] = 'true'
      end

      desc 'Responds with Recipes list',
           consumes: %w[application/json],
           success: { code: 200, model: Entities::Recipe, message: 'Recipe' },
           failure: request_failures
      get do
        present :collection, recipes_service.list, with: ::Entities::Recipe
      end

      route_param :id do
        desc 'Responds with Recipe details',
             consumes: %w[application/json],
             success: { code: 200, model: Entities::Recipe, message: 'Recipe' },
             failure: request_failures
        get do
          recipe = recipes_service.get(params[:id])
          raise ::Errors::NotFound unless recipe

          present recipe, with: ::Entities::RecipeDetails
        end
      end
    end
  end
end
