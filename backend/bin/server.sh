#!/bin/bash
echo "Starting backend"
set -ex

puma -b 'tcp://0.0.0.0:9393/' -t 8:32 config.ru
