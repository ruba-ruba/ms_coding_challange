# Backend Service #

#### Requirements

- ruby 2.7.*

#### Service Startup (without Docker)

> bundle install

> ./bin/server.sh


#### API

> curl localhost:9393/recipes

> curl localhost:9393/recipes/:id


#### Running tests

`bundle exec rspec`
