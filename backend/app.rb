# frozen_string_literal: true

APP_ROOT = __dir__

require_relative 'config/boot'
