import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RecipeListItem from './RecipeListItem'
import axios from 'axios';

export default class RecipeList extends React.Component {
  state = {
    recipes: []
  }

  componentDidMount() {
    axios.get(`http://localhost:9393/recipes`)
      .then(res => {
        const recipes = res.data.collection;
        this.setState({ recipes });
      })
  }

  render() {
      return (
        <div>
          <h4>Recipes</h4>
          { this.state.recipes.map(recipe => <div key={recipe.id}>{
            <RecipeListItem key={recipe.id} recipe={recipe} />
          }</div>) }
        </div>
      )
    }
}
