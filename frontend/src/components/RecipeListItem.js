import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, Route } from 'react-router-dom';

export class RecipeListItem extends Component {
  render() {
    const { id, title, image_url } = this.props.recipe
    return (
      <div className="recipe_list_item">
        <img src={image_url}/>
        <Link to={"/recipes/" + id}> {title} </Link>
      </div>
    )
  }
}

export default RecipeListItem