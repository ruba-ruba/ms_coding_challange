import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from "react-router";
import axios from 'axios';
import { Link } from 'react-router-dom';

export class RecipeDetails extends Component {
  state = {}

  componentDidMount() {
    const id = this.props.match.params.id;
    this.fetchRecipe(id);
   }

  fetchRecipe = id => {
    axios.get(`http://localhost:9393/recipes/${id}`)
      .then(res => {
        const recipe = res.data;
        this.setState({
          id: recipe.id,
          title: recipe.title,
          imageUrl: recipe.image_url,
          description: recipe.description,
          chefName: recipe.chef_name,
          tags: recipe.tags
        });
      })
   }

  render() {
    let tags;
    if(this.state.tags) {
      tags = <div><b>Tags:</b> {this.state.tags.map((tag, i) => <span key={tag+i}>{tag} </span>)} </div>
    }
    let chefName;
    if(this.state.chefName) {
      chefName = <div> <b>Chef Name: </b> { this.state.chefName } </div>
    }

    return (
      <React.Fragment>
        <h4>Recipe Details</h4>
        <div key={this.state.id} className='recipe_details'>
          <Link to={"/recipes"} className='return_link'> Return to list </Link>
          <div> <b>Recipe Name:</b> { this.state.title } </div>
          <div> <b>Description:</b> { this.state.description } </div>
          {chefName}
          {tags}
          <img src={this.state.imageUrl}/>
        </div>
      </React.Fragment>
    )
  }
}


export default withRouter(RecipeDetails);