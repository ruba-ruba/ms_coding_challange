import React from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import axios from 'axios';
import './App.css';
import RecipeList from './components/recipeList'
import RecipeDetails from './components/recipeDetails'

function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
            <Switch>
              <Route path="/recipes/:id/">
                <RecipeDetails />
              </Route>
              <Route path="/recipes">
                <RecipeList />
              </Route>
              <Route exact path="/">
                {<Redirect to="/recipes" />}
              </Route>
            </Switch>
        </header>
      </div>
    </Router>
  )
}


export default App