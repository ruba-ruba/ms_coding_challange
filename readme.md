#### Welcome to my version of [MS web challenge](https://gist.github.com/lawitschka/063f2e28bd6993cac5f8b40b991ae899)

##### about this app - framework selection

1. Backend: (Ruby/Grape): I guess that most of received code challanges are done using Rails. I've chosen Grape over Rails to make it more fun to review and to show a bit more alternative approach.

2. Frontend (ReactJS): I'm not frontend engineer hence React was obvious choice. Don't think i've followed best practices though i guess it is enough to show how backend works.

##### How to review this app

- I would suggest to spend most of the review time on backend only. As for the backend I can talk about code quality and architecture design choices. I'm rarely do frontend these days.

##### Requirements:

 - [docker](https://docs.docker.com/get-docker/)
 - [docker-compose](https://docs.docker.com/compose/install/)


##### Setup

 1. `docker-compose up --build`
 2. wait a moment
 3. service is running on `localhost:3000`

> .

> .

> .

##### Implementation requirements walkover:


> Create a sample web application that uses the Contentful API to fetch data

- done but not in the simplest way, though, would simple app be enough nowadays ?

> You can choose to write the application in Ruby and any of its frameworks

- done

> There should be 2 views: 1) a list view of all the recipes & 2) A detail view of a recipe

- done

> When clicking on a recipe on the list view, you should then show the detailed view

- done

> The application should be as production-ready as possible, but no deployment is needed

- partially done.
- Frontend is still running in dev mode, nginx would need to be configured & used to make it `production` ready. Backend on the other hand is ok.
- Also services are dockerized which shoukd make it easy to run locally or deploy to cloud.
